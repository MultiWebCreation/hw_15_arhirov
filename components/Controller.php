<?php
/**
 * Created by PhpStorm.
 * User: oculus
 * Date: 20.02.19
 * Time: 20:57
 */

abstract class Controller
{
    public $view;

    public function __construct()
    {
        $this->view = new View();
    }

    abstract public function actionIndex();
}