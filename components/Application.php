<?php
/**
 * Created by PhpStorm.
 * User: oculus
 * Date: 20.02.19
 * Time: 20:37
 */

final class Application
{
    public function run()
    {
        $requestUri = $_SERVER['REQUEST_URI'];

        $routes = explode('/', $requestUri);

        $controllerName = 'site'; // SiteController
        $actionName = 'index'; // actionIndex

        if (!empty($routes[1])) {
            $controllerName = $routes[1];
        }

        if (!empty($routes[2])) {
            $actionName = $routes[2];
        }

        $controllerName = ucfirst($controllerName) . 'Controller';
        $actionName = 'action' . ucfirst($actionName);

        $controllerPath = '../controllers/' . $controllerName . '.php';

        if (file_exists($controllerPath)) {
            require_once $controllerPath;
        } else {
            $this->error404();
        }

        $controller = new $controllerName();

        if (method_exists($controller, $actionName)) {
            if (isset($routes[3])) {
                $controller->$actionName($routes[3]);
            } else {
                $controller->$actionName();
            }
        } else {
            $this->error404();
        }
    }

    private function error404()
    {
        die('Page not found');
    }
}