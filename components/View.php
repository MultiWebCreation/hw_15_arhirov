<?php
/**
 * Created by PhpStorm.
 * User: oculus
 * Date: 24.02.19
 * Time: 16:34
 */

class View
{
    public function render($template, $params = [], $layout = 'main')
    {
        extract($params);
        include '../views/' . $layout . '.php';
    }

}