<?php

namespace components;

use PDO;
use PDOException;

class Model
{

    protected static $db;

    public function __construct()
    {
        $config = require '../config/db.php';
        try {
                self::$db = new PDO('mysql:host='. $config['host'] .';dbname='. $config['name'] .';charset=utf8;', $config['user'], $config['password']);

                self::$db->setAttribute(
                    PDO::ATTR_ERRMODE,
                    PDO::ERRMODE_EXCEPTION
                );
            } catch (PDOException $e) {
                echo 'Database connection error';
                die;
            }
    }

    public function query($sql) {
        $query = self::$db->query($sql);
        return $query;
    }

    public function row($sql) {
        $result = $this->query($sql);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function column($sql) {
        $result = $this->query($sql);
        return $result->fetchColumn();
    }

    public static function setTitle($data) {
        $title = $data;
        return $title;
    }

    public static function findAll($table)
    {
        $db = new Model();
        $data = $db->row('SELECT * FROM ' . $table);
        return $data;
    }

    public static function findOne($id, $table)
    {
        $db = new Model();
        $data = $db->row('SELECT * FROM '. $table . ' WHERE id = ' . $id);
        return $data;
    }

    public static function setOneTitle($id, $table) {
        $db = new Model();
        $data = $db->column('SELECT title FROM `'. $table .'` WHERE id = ' . $id);
        // $data .= '<title>' . $data . '</title>';
        return $data;
    }

}