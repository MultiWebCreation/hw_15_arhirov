<?php

use components\Model;

class SiteController extends Controller
{

    public function actionIndex()
    {
        $title = Model::setTitle('Главная');
        $this->view->render('site/index', [
            'title' => $title,
        ]);
    }
}