<?php

require_once '../models/Post.php';
//use models\Post;
use components\Model;

class PostController extends Controller
{

    public function actionIndex()
    {
//        $db = new Model();
//        $data = $db->row('SELECT * FROM `articles`');
        $data = Model::findAll('posts');
        $title = Model::setTitle('Новости');
        //var_dump($data);
        $this->view->render('post/index', [
            'articles' => $data,
            'title' => $title,
        ]);

    }

    public function actionView($id)
    {
        $data = Model::findOne($id, 'posts');
        $title = Model::setOneTitle($id, 'posts');
//        $db = new Model();
//        $data = $db->row('SELECT * FROM `articles` WHERE id = ' . $id);
        $this->view->render('post/view', [
            'post' => $data,
            'title'=> $title,
        ]);
    }
}