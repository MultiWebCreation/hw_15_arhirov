<?php

require_once '../models/Article.php';
use models\Article;
use components\Model;

class ArticleController extends Controller
{

    public function actionIndex()
    {
//        $db = new Model();
//        $data = $db->row('SELECT * FROM `articles`');
        $data = Model::findAll('articles');
        $title = Model::setTitle('Статьи');
        //$title = Article::setTitle('Статьи');
        //var_dump($data);
        $this->view->render('article/index', [
            'articles' => $data,
            'title' => $title,
        ]);

    }

    public function actionView($id)
    {
        $data = Model::findOne($id, 'articles');
        $title = Model::setOneTitle($id, 'articles');
//        $db = new Model();
//        $data = $db->row('SELECT * FROM `articles` WHERE id = ' . $id);
        $this->view->render('article/view', [
            'post' => $data,
            'title'=> $title,
        ]);
    }
}