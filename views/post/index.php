<div class="col-md-12">
    <h1 class="ttl"><?=$title?></h1>
</div>

<?php foreach ($articles as $item) : ?>
    <div class="col-md-6 item">
        <h3><?=$item['title']?></h3>
        <p class="text-justify"><?=$item['short']?></p>
        <a href="post/view/<?=$item['id']?>" class="btn btn-success">Подробнее</a>
    </div>
<?php endforeach; ?>